import argparse
import json
import re
from enum import Enum
from logging import Logger
from pathlib import Path
from typing import Optional, Union

import yaml
from pydantic import BaseModel

LOGGER = Logger(__name__)


class AnsiColors:
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    YELLOW = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    WHITE = "\033[0;37m"

    HGRAY = "\033[90m"
    HRED = "\033[91m"
    HGREEN = "\033[92m"
    HYELLOW = "\033[93m"
    HBLUE = "\033[94m"
    HPURPLE = "\033[95m"
    HCYAN = "\033[96m"
    HWHITE = "\033[97m"

    RESET = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class GlInputType(str, Enum):
    """GitLab CI/CD component input type."""

    string = "string"
    boolean = "boolean"
    number = "number"


class TbcVarType(str, Enum):
    """to-be-continuous variable type."""

    text = "text"
    boolean = "boolean"
    number = "number"
    enum = "enum"
    url = "url"

    def to_gl(self) -> GlInputType:
        if self == TbcVarType.boolean:
            return GlInputType.boolean
        if self == TbcVarType.number:
            return GlInputType.number
        return GlInputType.string


class GlInput(BaseModel):
    # name: str
    description: Optional[str] = None
    type: GlInputType = GlInputType.string
    options: Optional[list[str]] = None
    default: Optional[Union[str, bool, int]] = None


class TbcVar(BaseModel):
    name: str
    description: Optional[str] = None
    type: TbcVarType = TbcVarType.text
    values: Optional[list[str]] = None
    default: Optional[str] = None
    advanced: bool = False
    secret: bool = False
    mandatory: bool = False

    def input_name(self, var_prefix: str) -> str:
        return (
            (
                self.name[len(var_prefix) :]
                if self.name.startswith(var_prefix)
                else self.name
            )
            .replace("_", "-")
            .lower()
        )

    def to_gl(self) -> GlInput:
        return GlInput(
            # TODO: remove Markdown formatting?
            description=self.description,
            type=self.type.to_gl(),
            default=self.gl_dflt,
            options=self.values,
        )

    @property
    def gl_dflt(self):
        if self.type == TbcVarType.boolean:
            # return bool(self.default) if self.default != None else False
            return self.default or "false"
        if self.type == TbcVarType.number:
            # return int(self.default) if self.default != None else 0
            return self.default or "0"
        return self.default or ""


class DocVar(BaseModel):
    lock: bool
    var_name: str
    input_name: Optional[str] = None
    description: str
    default_cell: str

    def default(self, type: GlInputType) -> Optional[str]:
        code_expr_match = re.match(r"`([^`]*)`", self.default_cell)
        explicit_default = code_expr_match.group(1) if code_expr_match else None

        if type == GlInputType.boolean:
            # return bool(self.default) if self.default != None else False
            return explicit_default or "false"
        if type == GlInputType.number:
            # return int(self.default) if self.default != None else 0
            return explicit_default or "0"
        return explicit_default or ""


def _get_var(tpl_desc: dict[str, any], var_name: str) -> Optional[TbcVar]:
    var = next(
        iter(
            [
                TbcVar.parse_obj(var)
                for var in tpl_desc.get("variables", [])
                if var["name"] == var_name
            ]
        ),
        None,
    )
    if var:
        return var

    # look into feature variables
    for feat in tpl_desc.get("features", []):
        if feat.get("enable_with") == var_name:
            return TbcVar(
                name=feat.get("enable_with"),
                description=f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
        elif feat.get("disable_with") == var_name:
            return TbcVar(
                name=feat.get("disable_with"),
                description=f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
        var = next(
            iter(
                [
                    TbcVar.parse_obj(var)
                    for var in feat.get("variables", [])
                    if var["name"] == var_name
                ]
            ),
            None,
        )
        if var:
            return var

    return None


def _check_var(
    tbc_var: TbcVar,
    var_prefix: str,
    tpl_spec: dict[str, any],
    tpl_impl: dict[str, any],
    main_tpl_desc: Optional[dict[str, any]],
    doc_vars: list[DocVar],
) -> int:
    has_no_input = (
        tbc_var.secret
        or main_tpl_desc
        and _get_var(main_tpl_desc, tbc_var.name)
        or tbc_var.name.startswith("TBC_")
    )
    expected_input_name = tbc_var.input_name(var_prefix)
    expected_gl_input = tbc_var.to_gl()
    # check variable is declared in doc
    doc_var = next(filter(lambda dv: dv.var_name == tbc_var.name, doc_vars), None)
    if doc_var == None:
        print(
            f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: not documented in README{AnsiColors.RESET}"
        )
    else:
        # check default
        if doc_var.default(expected_gl_input.type) != expected_gl_input.default:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: README default ({doc_var.default(tbc_var.type)}) doesn't match Kicker's ({expected_gl_input.default}){AnsiColors.YELLOW}"
            )

        if doc_var.lock and not tbc_var.secret:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: is not declared as a secret but has a lock in README{AnsiColors.RESET}"
            )
        elif not doc_var.lock and tbc_var.secret:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: is declared as a secret but has no lock in README{AnsiColors.RESET}"
            )
        elif not has_no_input and expected_input_name != doc_var.input_name:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: has wrong input declared in README ({doc_var.input_name}){AnsiColors.RESET}"
            )

    # retrieve declared input from template specs
    declared_input = tpl_spec["spec"]["inputs"].get(expected_input_name)

    if tbc_var.secret:
        # secrets should not be inputs
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is a secret: must not be declared{AnsiColors.RESET}"
            )
            return 1
        else:
            print(
                f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is a secret: skip{AnsiColors.RESET}"
            )
            return 0
    if main_tpl_desc and _get_var(main_tpl_desc, tbc_var.name):
        # a variant is overriding a variable from the main template: skip
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is an override: must not be declared{AnsiColors.RESET}"
            )
            return 1
        else:
            print(
                f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is an override: skip{AnsiColors.RESET}"
            )
            return 0
    if tbc_var.name.startswith("TBC_"):
        # global TBC variable: skip
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is global TBC: must not be declared{AnsiColors.RESET}"
            )
            return 1
        else:
            print(
                f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is global TBC: skip{AnsiColors.RESET}"
            )
            return 0

    # check if mapped GitLab input is declared
    if not declared_input:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: input not found{AnsiColors.RESET}"
        )
        return 1

    err_count = 0
    # check actual input is as expected
    actual_gl_input = GlInput.parse_obj(declared_input)

    if actual_gl_input.type != expected_gl_input.type:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: type ({actual_gl_input.type}) doesn't match Kicker's ({expected_gl_input.type}){AnsiColors.RESET}"
        )
        err_count += 1

    if actual_gl_input.description != expected_gl_input.description:
        print(
            f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: description doesn't match Kicker's{AnsiColors.RESET}"
        )

    if actual_gl_input.default != expected_gl_input.default:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: default ({actual_gl_input.default}) doesn't match Kicker's ({expected_gl_input.default}){AnsiColors.RESET}"
        )
        err_count += 1

    if actual_gl_input.options != expected_gl_input.options:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: options ({actual_gl_input.options}) doesn't match Kicker's ({expected_gl_input.options}){AnsiColors.RESET}"
        )
        err_count += 1

    # check variable is initialized from input
    actual_variable_value = tpl_impl["variables"].get(tbc_var.name)
    expected_variable_value = f"$[[ inputs.{expected_input_name} ]]"
    if not actual_variable_value:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: variable not declared{AnsiColors.RESET}"
        )
        err_count += 1
    elif actual_variable_value != expected_variable_value:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: value ({actual_variable_value}) doesn't match Kicker's ({expected_variable_value}){AnsiColors.RESET}"
        )
        err_count += 1

    if err_count == 0:
        print(
            f"  {AnsiColors.GREEN}✓{AnsiColors.RESET} <{tbc_var.name}/{expected_input_name}>: OK"
        )

    return err_count


def _check_tpl(
    tpl_desc, main_tpl_desc, project_dir: Path, var_prefix: str, doc_vars: list[DocVar]
) -> int:
    tpl_path = project_dir / tpl_desc["template_path"]
    if not tpl_path.exists():
        print(f"{AnsiColors.RED}ERROR: Template file ({tpl_path}) not found: abort")
        exit(1)

    # load template
    with open(tpl_path, "r") as reader:
        all_tpl = list(yaml.load_all(reader, Loader=yaml.BaseLoader))
        tpl_spec = all_tpl[0]
        tpl_impl = all_tpl[-1]

    inputs: dict[str, dict[str, any]] = dict(tpl_spec["spec"]["inputs"])
    err_count = 0
    # check main template variables
    for var in tpl_desc.get("variables", []):
        tbc_var = TbcVar.parse_obj(var)
        err_count += _check_var(
            tbc_var, var_prefix, tpl_spec, tpl_impl, main_tpl_desc, doc_vars
        )
        input_name = tbc_var.input_name(var_prefix)
        if input_name in inputs:
            del inputs[input_name]

    # check feature variables
    for feat in tpl_desc.get("features", []):
        if feat.get("enable_with"):
            tbc_var = TbcVar(
                name=feat.get("enable_with"),
                description=f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_impl, main_tpl_desc, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]
        elif feat.get("disable_with"):
            tbc_var = TbcVar(
                name=feat.get("disable_with"),
                description=f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_impl, main_tpl_desc, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]
        for var in feat.get("variables", []):
            tbc_var = TbcVar.parse_obj(var)
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_impl, main_tpl_desc, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]

    # remaining inputs are unmapped inputs
    for input_name in inputs:
        print(
            f"  {AnsiColors.RED}✕ unmapped declared input '{input_name}'{AnsiColors.RESET}"
        )

    err_count += len(inputs)
    return err_count


def _load_vars_from_doc(project_dir: Path) -> list[DocVar]:
    # read file
    readme_path = project_dir / "README.md"
    if not readme_path.exists():
        print(f"{AnsiColors.RED}ERROR: Readme file ({readme_path}) not found: abort")
        exit(1)
    with open(readme_path, "r") as reader:
        readme_content = reader.read()

    # group 1: ':lock:'
    # group 2: <name>
    # group 3 (optional): <name>
    # group 4: <description>
    # group 5: <default>
    var_doc_regex = re.compile(
        r"^\| *(:lock:)? *`([a-zA-Z0-9-_]+)`(?: *\/ *`([a-zA-Z0-9-_]+)`)? *\| *([^|]*) *\| *([^|]*)\ *\|$",
        flags=re.MULTILINE,
    )
    return [
        DocVar(
            lock=bool(match.group(1)),
            var_name=match.group(3) or match.group(2),
            input_name=match.group(2) if match.group(3) else None,
            description=match.group(4),
            default_cell=match.group(5),
        )
        for match in var_doc_regex.finditer(readme_content)
    ]


def run():
    # define command parser
    parser = argparse.ArgumentParser(
        prog="tbc-check",
        description="This tool performs several checks on a to-be-continuous template project",
    )
    parser.add_argument("project_dir", default=".")

    # parse command and args
    args = parser.parse_args()

    project_dir = Path(args.project_dir)
    if not project_dir.exists():
        print(
            f"{AnsiColors.RED}ERROR: Project path ({project_dir}) does not exist: abort"
        )
        exit(1)
    if not project_dir.is_dir():
        print(
            f"{AnsiColors.RED}ERROR: Project path ({project_dir}) is not a directory: abort"
        )
        exit(1)

    # load kicker file
    kicker_path = project_dir / "kicker.json"
    if not kicker_path.exists():
        print(f"{AnsiColors.RED}ERROR: Kicker file ({kicker_path}) not found: abort")
        exit(1)

    with open(kicker_path, "r") as reader:
        kicker = json.load(reader)

    # load vars from README
    doc_vars = _load_vars_from_doc(project_dir)

    # retrieve prefix from kicker or guess it from first XXX_IMAGE variable
    prefix: str = (
        kicker.get("prefix")
        or next(
            map(
                lambda var: var["name"].split("_")[0],
                filter(
                    lambda var: var["name"].endswith("_IMAGE"),
                    kicker.get("variables", []),
                ),
            ),
            None,
        )
        or kicker["name"].lower()
    )
    var_prefix = prefix.upper() + "_"

    print("=============================================================")
    print(
        f"Checking template {AnsiColors.CYAN}{kicker['name']}{AnsiColors.RESET} (vars prefix {AnsiColors.CYAN}\"{var_prefix}\"{AnsiColors.RESET})"
    )
    print("=============================================================")
    if not kicker.get("is_component"):
        print(
            f"{AnsiColors.HGRAY}✕ this project is not a CI/CD component: skip{AnsiColors.RESET}"
        )
        return

    # Check main template
    print(
        f"{AnsiColors.BLUE}{AnsiColors.BOLD}→ Main template ({kicker['template_path']}){AnsiColors.RESET}"
    )
    err_count = _check_tpl(kicker, None, project_dir, var_prefix, doc_vars)

    # Check variants
    for variant in kicker.get("variants", []):
        print(
            f"{AnsiColors.BLUE}{AnsiColors.BOLD}→ {variant['name']} variant ({variant['template_path']}){AnsiColors.RESET}"
        )
        err_count += _check_tpl(variant, kicker, project_dir, var_prefix, doc_vars)

    if err_count > 0:
        exit(127)
