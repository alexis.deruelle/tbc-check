# tbc-check CLI tool

`tbc-check` is a basic CLI program that checks to-be-continuous templates.

## Usage

```bash
# install dependencies
poetry install

# obtain help
poetry run tbc-check --help

# run tool
poetry run tbc-check path/to/tbc/template
```

## Checked rules

`tbc-check` checks the following rules:

- the existence of the `kicker.json` file
- the existence of the `README.md` file
- for each variable in the `kicker.json` file (main template and variants):
    - mapped input is declared in the related template with same name, type, default, options and description (WARNING only)
    - the variable is declared in the related template with expected input mapping (`$[[ inputs.input-name ]]`)
    - the input/variable is documented in the README with same default (WARNING only)

If any non-WARNING rule fails, `tbc-check` fails with exit code 127.
